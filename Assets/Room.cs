﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class Room : MonoBehaviour
{
    public List<GameObject> projectile;
    public List<Enemy> enemiesInRoom = new List<Enemy>();
    public string cameraAnimationToMoveToNextRoom;
    public float animationLength = 1f;
    public Room nextRoom;
    public float throwDelay = 0.6f;
    public bool isActive = false;
    public void EnemyKilled(Enemy enemy)
    {
        enemiesInRoom.Remove(enemy);
        Debug.Log(enemiesInRoom.Count);
        if (enemiesInRoom.Count == 0)
            StartCoroutine(RoomComplete());
    }

    public void StartRoom()
    {
        isActive = true;
        foreach (Enemy enemy in enemiesInRoom)
        {
            enemy.timeBetweenShots = throwDelay * 1.1f;
            enemy.Wakeup();
            enemy.room = this;
        }
    }

    [ContextMenu("RoomComplete")]
    void DebugComp()
    {
        foreach (Enemy enemy in enemiesInRoom)
        {
            enemy.gameObject.SetActive(false);
        }
        StartCoroutine(RoomComplete());
    }

    public bool isLastRoom = false;
    IEnumerator RoomComplete()
    {
        Debug.Log("Room complete");
        Animator camAnim = Camera.main.gameObject.GetComponent<Animator>();
        isActive = false;
        camAnim.Play(cameraAnimationToMoveToNextRoom);
        yield return new WaitForSeconds(animationLength);
        if (!isLastRoom)
        {
            GameObject.FindObjectOfType<Gamemanager>().SetRoom(nextRoom);
            GameObject.FindObjectOfType<Gamemanager>().StartRoom();
        }
    }
}
