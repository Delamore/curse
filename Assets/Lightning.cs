﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning : MonoBehaviour
{
    public Color Dark;

    public Color Light;
    public AudioClip thunder;
    public AudioClip lightning;
    private AudioSource audio;
    private float timeBetweenLighting = 3f;
    private float randomAdjustToTime = 15f;

    private bool canLightning = true;
    private float timeBetweenChecks = 1f;
    private float nextLightningTime;

    private int maxLightning = 3;
    private float maxTimeBtweenLighting = 2f;
    private float minTimeBetweenLighting = 0.8f;
    private float timeForFlash = 0.17f;
    private float thunderTime = 0.3f;
    private bool striking = false;
    private Light light;
    void Awake()
    {
        light = GetComponent<Light>();
        audio = GetComponent<AudioSource>( );
        SetNextLightningTime();
    }
    void Update()
    {
        if (!striking && Time.timeSinceLevelLoad > nextLightningTime)
            StartCoroutine(Strike());
    }

    void SetNextLightningTime()
    {
        float nextTime = timeBetweenLighting + Random.Range(0, randomAdjustToTime);
        nextLightningTime = nextTime + Time.timeSinceLevelLoad;
    }

    IEnumerator Strike()
    {
        striking = true;
        int timesToStrike = Random.Range(1, maxLightning);
        for (int i = 0; i < timesToStrike; i++)
        {
            AudioSource.PlayClipAtPoint(thunder,Camera.main.transform.position);
            yield return new WaitForSeconds(thunderTime);
            AudioSource.PlayClipAtPoint(lightning, Camera.main.transform.position,0.3f);
            FlashLight(Light);
            yield return new WaitForSeconds(timeForFlash);
            FlashLight(Dark);
            yield return new WaitForSeconds(Random.Range(minTimeBetweenLighting,maxTimeBtweenLighting));
        }

        SetNextLightningTime();

        striking = false;
    }

    void FlashLight(Color color)
    {
        light.color = color;
    }
}
