﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Material mat;
    //private MeshRenderer rend;
    //private static Material flashMat => _flashMat ?? (_flashMat = Resources.Load<Material>("Materials/flash"));
    //private static Material _flashMat;
    public List<EnemyProjectile> projectiles;
    private float timeLastShot = 0;
    public float timeBetweenShots = .7f;
    private float randomTimeBetweenOffset = 0.2f;
    private Gamemanager gamemanager;
    private bool isAwake = false;
    public Room room;
    void Awake()
    {
        //rend = GetComponent<MeshRenderer>();
        //mat = rend.material;
        foreach (EnemyProjectile proj in projectiles)
            proj.enemy = this;
        gamemanager = FindObjectOfType<Gamemanager>();
        gamemanager.enemiesRemaining += projectiles.Count;
    }

    void Update()
    {
        if (!isAwake)
            return;
        if (projectiles.Count == 0)
        {
            Break();
        }
    }

    public void Wakeup()
    {
        isAwake = true;
    }
    //void OnCollisionEnter(Collision coll)
    //{
    //    Projectile proj = coll.other.GetComponent<Projectile>();
    //    if (proj != null && !proj.hasHit)
    //    {
    //        proj.Hit();
    //        TakeDamage();
    //    }
    //}

    public bool CanShoot()
    {
        return projectiles.Count > 0 && Time.timeSinceLevelLoad - timeLastShot > timeBetweenShots;
    }
    public void Shoot()
    {
        gamemanager.timeEnemyProjectileLastThrown = Time.timeSinceLevelLoad;
        timeLastShot = Time.timeSinceLevelLoad - Random.Range(0, randomTimeBetweenOffset);
        EnemyProjectile proj = GetRandomProjectile();
        proj.Shoot();
    }

    EnemyProjectile GetRandomProjectile()
    {
        int rand = Random.Range(0, projectiles.Count);
        EnemyProjectile proj = projectiles[rand];
        //projectiles.RemoveAt(rand);
        return proj;
    }
    //void TakeDamage()
    //{
    //    StartCoroutine(Flash());
    //}
    //IEnumerator Flash()
    //{
    //    rend.material = flashMat;
    //    yield return new WaitForSeconds(0.1f);
    //    rend.material = mat;
    //}

    public void ProjectileBroken(EnemyProjectile proj)
    {
        projectiles.Remove(proj);
        FindObjectOfType<Gamemanager>().enemiesRemaining--;
    }
    void Break()
    {
        room.EnemyKilled(this);
        gameObject.SetActive(false);
    }
}
