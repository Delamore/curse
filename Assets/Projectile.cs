﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public bool hasHit = false;
    public float throwForce = 5000;
    private float despawnTimer = 5f;
    private Rigidbody rb;
    public void Shoot(Vector3 startPos, Vector3 direction)
    {
        transform.position = startPos;
        rb = GetComponent<Rigidbody>();
        rb.mass = 5f;
        rb.AddForce(GetForce(startPos, direction));
        rb.AddTorque(Random.onUnitSphere);
        rb.useGravity = false;
        StartCoroutine(Despawn());
    }

    public void Hit()
    {
        hasHit = true;
    }
    Vector3 GetForce(Vector3 startpos, Vector3 direction)
    {
        return direction * throwForce;
    }

    IEnumerator Despawn()
    {
        yield return new WaitForSeconds(despawnTimer);
        DestroyImmediate(gameObject);
    }

    void OnCollisionEnter(Collision coll)
    {
        rb.useGravity = true;
    }
}
