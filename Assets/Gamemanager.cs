﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class Gamemanager : MonoBehaviour
{
    public int enemiesRemaining;
    public List<GameObject> projectile;
    private int HP = 5;
    public float timeEnemyProjectileLastThrown = 0;
    float minTimeBetweenThrow = 0.6f;
    float shotDelay = 0.25f;
    private float lastShotTime = 0;
    public Image redFlash;
    public Room currentRoom;

    public bool CanEnemiesShoot()
    {
        bool can = (Time.timeSinceLevelLoad - timeEnemyProjectileLastThrown) > minTimeBetweenThrow;
        //if(can)
            //Debug.Log($"{Time.timeSinceLevelLoad - timeEnemyProjectileLastThrown}");
        return can;
    }

    public void SetThrowSpeed(float speed)
    {
        minTimeBetweenThrow = speed;
    }
    //void Awake()
    //{
    //    originalCameraPos = Camera.main.transform.position;
    //    SetThrowSpeed(currentRoom.throwDelay);
    //}

    void Start()
    {
        originalCameraPos = Camera.main.transform.position;
        StartRoom();
        SetThrowSpeed(currentRoom.throwDelay);
    }
    void Update()
    {
        CheckMouseClick();
        if (CanEnemiesShoot())
        {
            int rand = Random.Range(0, currentRoom.enemiesInRoom.Count);
            if (currentRoom.isActive && currentRoom.enemiesInRoom[rand].CanShoot()) 
                currentRoom.enemiesInRoom[rand].Shoot();
        }
    }

    public void StartRoom()
    {
        originalCameraPos = Camera.main.transform.position;
        Debug.Log(currentRoom);
        currentRoom.StartRoom();
        HP = 5;
        GameObject.FindObjectOfType<Health>().LoseHP(5);
    }

    public void SetRoom(Room room)
    {
        Debug.Log("set" + room.name);
        currentRoom = room;
    }
    void CheckMouseClick()
    {
        if (Time.timeSinceLevelLoad - lastShotTime < shotDelay)
            return;
        if (Input.GetMouseButtonDown(0))
        {
            //Debug.Log("Click");
            //LayerMask mouseClickCollisonLayer = 1 << 9;
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //Debug.DrawLine(ray.origin, ray.direction * 1000, Color.red, 5f);
            //if (Physics.Raycast(ray, out hit, 1000, mouseClickCollisonLayer))
            //{
            //    Debug.Log(hit.transform.gameObject.name);
            //    Debug.DrawLine(ray.origin, hit.point, Color.blue, 5f);
            //}
            Shoot(ray.origin, ray.direction);
        }
    }

    public bool dead = false;
    public void HPLoss()
    {
        if (dead)
            return;
        if (HP == 0)
        {
            dead = true;
            StartCoroutine(GameOver());
            return;
        }

        StopCoroutine(ScreenShake());
        StartCoroutine(FlashRed());
        StartCoroutine(ScreenShake());
        HP--;
        FindObjectOfType<Health>().LoseHP(HP);
        if (HP == 0)
            StartCoroutine(GameOver());
    }

    public GameObject gameOver;
    IEnumerator GameOver()
    {
        gameOver.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        while (!Input.anyKey)
        {
            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene(0);
    }

    IEnumerator FlashRed()
    {
        redFlash.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        redFlash.gameObject.SetActive(false);
    }
    private float screenShakeLength = 0.2f;
    private float screenShakeAmount = 0.02f;
    private Vector3 originalCameraPos;
    IEnumerator ScreenShake()
    {
        Transform cam = Camera.main.transform;
        Vector3 offset = Random.onUnitSphere * screenShakeAmount;
        Vector3 pos = cam.position + offset;
        float shakingTime = screenShakeLength;
        float percent = screenShakeLength / shakingTime;
        while (percent > 0)
        {
            yield return new WaitForEndOfFrame();
            shakingTime -= Time.deltaTime;
            percent = shakingTime / screenShakeLength;
            Camera.main.transform.position = Vector3.Lerp(originalCameraPos, pos, percent);
        }
    }
    void Shoot(Vector3 startPos, Vector3 direction)
    {
        lastShotTime = Time.timeSinceLevelLoad;
        GameObject project = Instantiate(GetRandomProjectile());
        Projectile proj = project.AddComponent<Projectile>();
        proj.Shoot(startPos, direction);
    }

    GameObject GetRandomProjectile()
    {
        int random = Random.Range(0, currentRoom.projectile.Count);

        //Debug.Log(random);
        return currentRoom.projectile[random];
    }

}
