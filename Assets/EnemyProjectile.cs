﻿using System.Collections;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
public class EnemyProjectile : MonoBehaviour
{
    public bool hasHit = false;

    public Transform center;
    public Rigidbody rb;
    private static Material flashMat => _flashMat ?? (_flashMat = Resources.Load<Material>("Materials/levitateflash"));
    private static Material _flashMat;
    private static ParticleSystem trailParticles => _trailParticles ?? (_trailParticles = Resources.Load<ParticleSystem>("Particles/trail"));
    private static ParticleSystem _trailParticles;

    public AudioClip collideSound;
    private AudioSource audio;
    private Material[] originalMaterials;
    private MeshRenderer rend;
    public Vector3 originalPosition;
    public bool hasBeenShot = false;
    public bool isMovingBackToPosition = false;
    public bool isCurrentlyFlyingToPlayer = false;
    public Enemy enemy;
    private ParticleSystem particles;

    public int HP = 1;
    private float flashLength = 0.35f;
    private float levitatePower = 15;
    float throwForce = 400;
    private float levitateTime = 0.5f;
    void Awake()
    {
        audio = GetComponent<AudioSource>();
        particles = Instantiate(trailParticles, transform);
        if(center != null)
            particles.transform.position = center.transform.position;
        else
            particles.transform.position = transform.position;
        particles.Stop();
        rend = GetComponent<MeshRenderer>();
        originalMaterials = rend.materials;
        rb = GetComponent<Rigidbody>();
        originalPosition = transform.position;
    }


    void Update()
    {
        if (!isMovingBackToPosition && !IsCloseToSpawnPos() && !isCurrentlyFlyingToPlayer)
        {
            //Debug.Log("Returning due to distance and nolonger flying to player");
            StartMovingBack();
        }
        if (isMovingBackToPosition)
            ContinueMovingBackToPosition();
    }

    private float timeSpentMovingBack => Time.time - timeStartedMovingBack;
    private float timeStartedMovingBack = 0;
    void StartMovingBack()
    {
        if (isMovingBackToPosition)
            return;
        isMovingBackToPosition = true;
        timeStartedMovingBack = Time.timeSinceLevelLoad;

    }
    void ContinueMovingBackToPosition()
    {
        if (IsCloseToSpawnPos())
        {
            rb.velocity = Vector3.zero;
            hasBeenShot = false;
            isMovingBackToPosition = false;
        }
        float dist = Vector3.Distance(transform.position, originalPosition);
        if (timeSpentMovingBack > 2f)
        {
            rb.AddForce(Random.onUnitSphere * 5 * timeSpentMovingBack);
        }
        else
        {
            Vector3 speed = originalPosition - transform.position;
            if (dist < 0)
                speed *= dist - 1;
            else
                speed *= dist + 1;
            rb.velocity = speed;
            rb.AddForce(Vector3.up *2f);
        }
    }

    bool IsCloseToSpawnPos()
    {
        float dist = Vector3.Distance(transform.position, originalPosition);
        if (dist < .5f)
        {
            return true;
        }
        else return false;
    }
    public void Shoot()
    {
        if (hasBeenShot )
        {
            Debug.Log("Movin back because attempted to shoot while shot");
            StartMovingBack();
            return;
        }
        hasBeenShot = true;
        StartCoroutine(Levitate());
    }

    void ShootTowardsPlayer()
    {
        Vector3 direction = GetDirectionToPlayer();
        Vector3 startPos = transform.position;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.AddForce(GetForce(startPos, direction));
        rb.AddTorque(Random.onUnitSphere);
        StartCoroutine(StopParticles());
    }

    private float lastCollidetime = 0;
    private float minCollideTime = 0.2f;
    private float minVelocity = 0.1f;
    void OnCollisionEnter(Collision coll)
    {
        //Debug.Log(rb.velocity.magnitude);
        if (Time.timeSinceLevelLoad - lastCollidetime > minCollideTime && rb.velocity.magnitude > minVelocity)
        {
            lastCollidetime = Time.timeSinceLevelLoad;
            
            audio.PlayOneShot(collideSound,0.2f * Mathf.Log(rb.velocity.magnitude));
        }
        Projectile proj = coll.other.GetComponent<Projectile>();
        if (proj != null && !proj.hasHit)
        {
            HP--;
            proj.Hit();
            if (HP == 0)
                Break();
        }
    }

    IEnumerator StopParticles()
    {
        yield return new WaitForSeconds(0.35f);
        particles.Stop();
    }
    void Break()
    {
        StopAllCoroutines();
        gameObject.SetActive(false);
        enemy.ProjectileBroken(this);
        //Destroy(this.gameObject);
    }
    IEnumerator Levitate()
    {
        particles.Play();
        isCurrentlyFlyingToPlayer = true;
        StartCoroutine(Flash());
        rb.useGravity = false;
        rb.AddForce(Vector3.up * levitatePower);
        rb.mass = 5;
        yield return new WaitForSeconds(levitateTime);
        rb.velocity = Vector3.zero;
        StopCoroutine(TimerUntillStoppedFlyingAtPlayer());
        StartCoroutine(TimerUntillStoppedFlyingAtPlayer());
        ShootTowardsPlayer();
    }
    Vector3 GetDirectionToPlayer()
    {
        return (Camera.main.transform.position - (Vector3.down *-0.2f)) - transform.position;
    }
    public void Hit()
    {
        hasHit = true;
    }
    Vector3 GetForce(Vector3 startpos, Vector3 direction)
    {
        return direction * throwForce;
    }
    IEnumerator Flash()
    {
        //Debug.Log(gameObject.name);
        Material[] flashmats = new Material[rend.materials.Length];
        //Debug.Log($"{flashmats.Length} flash length");
        for (int i = 0; i < rend.materials.Length; i++)
        {
            flashmats[i] = flashMat;
        }
        SetMats(flashmats);
        rend.material = flashMat;
        yield return new WaitForSeconds(flashLength);
        SetMats(originalMaterials);
    }

    public bool canHit = true;
    public IEnumerator HitPlayer()
    {
        canHit = false;
        yield return new WaitForSeconds(0.2f);
        canHit = true;
    }
    IEnumerator TimerUntillStoppedFlyingAtPlayer()
    {
        yield return new WaitForSeconds(3f);
        isCurrentlyFlyingToPlayer = false;
        rb.mass = 1;
    }
    void SetMats(Material[] newMat)
    {
        rend.materials = newMat;
    }
}
