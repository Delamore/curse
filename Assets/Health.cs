﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Health : MonoBehaviour
{
    public TMP_Text text;

    public void LoseHP(int hp)
    {
        text.text = "";
        for (int i = hp; i > 0; i--)
        {
            text.text += "♥";
        }
    }
}
