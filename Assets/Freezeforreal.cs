﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Freezeforreal : MonoBehaviour
{
    private Vector3 originalPos;
    
    void Awake()
    {
        originalPos = transform.position;
    }

    void FixedUpdate()
    {
        transform.position = originalPos;
    }
}
