﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    private Gamemanager gamemanager;

    void Awake()
    {
        gamemanager = GameObject.FindObjectOfType<Gamemanager>();
    }
    public AudioSource audio;
    void OnCollisionEnter(Collision coll)
    {
        if (gamemanager.dead)
            return;
        EnemyProjectile proj = coll.other.GetComponent<EnemyProjectile>();
        if (proj != null && proj.canHit && proj.rb.velocity.magnitude > 0.5f)
        {
            
            GameObject.FindObjectOfType<Gamemanager>().HPLoss();
            StartCoroutine(proj.HitPlayer());
            audio.Play();
        }
    }
}
