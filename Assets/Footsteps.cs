﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    public AudioSource audio;

    public void StartSteps()
    {
        Debug.Log("play steps");
        audio.Play();
        
    }

    public void StopSteps()
    {
        audio.Stop();
    }
}
